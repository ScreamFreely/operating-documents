# Organizational Structure
All team members will be held to the standards stated in the Code of Conduct.

ScreamFreely (SF) is a non-profit run by two primary co-directors. One director is
focused on the fidelity of the digital systems SF is responsible for; the other
is focused on the organizational relationships and systems which maintain SF. Both
directors are responsible for the inter-personal and community well-being of the
organization.

## Activist Org Site Structure
Activist Org Site (AOS) structures will mimick SF organizational structure by requiring
each organization be run by two co-directors. Directors will be required to be of
differing racial and gender identities. Again, one director will focus on the digital
systems, and the other on the social systems. 

### Teams
AOS teams will be composed of between 3 and 5 people. Each state will have between
3 and 7 teams at maturity.

#### Team Lead
Responsibilities: 

- [ ] Priotize and track work load
- [ ] Support other team members

#### Team Member
Responsibilities: 

- [ ] Maintain server & systems
- [ ] Build additional scrapers
- [ ] Maintain current scrapers
- [ ] Provide client support
- [ ] Help Develop new features