# SF Call for Support
We are a non-profit because we want to be accountable to the communities we serve
and not allow profits to be a metric of our success. We will require the support
of our communities to do this work. There are three primary areas of support
that we are most in need of right now **financial**, **outreach**, and **tech** support.

## Financial Support
*Contact:* [Katie](mailto:katie@screamfreely.org)

This is a primary need at present. Katie has just concluded helping Angela Conley run
her campaign for Hennepin County Commissioner. We do hope to receive the ability 
to independently apply for grants. As well, we will begin a pilot program for 
organizations to join in Minnesota.

As soon as the capacity exists to expand to other states we hope to do so.

- [ ] Monthly Donations
- [ ] Grant Writing

## Outreach Support
*Contact:* [Katie](mailto:katie@screamfreely.org)

We aspire to have three primary markets for outreach, the community, organizations
within the community, and artists within the community. We will support the organizations
doing the work, and their communities, by providing easy access to information about
the officials, and institutions that govern us. We will support artists, whose voices 
are pivotal in helping us see our live through new lenses.

- [ ] Event Organizing
- [ ] Org Outreach
- [ ] UX Outreach

## Tech Support
*Contact:* [Canin](mailto:canin@screamfreely.org)

Of note, I have started building Docker scripts, with the aspiration of running Kubes.
Beyond the trend, we will seek to separate accounts, core-data, search, and file-service; 
kubernetes, at present, seems the best way forward. We are open to suggestions.

Internally there are a few features to finish up, the primary feature being the ability
for organizations to contact individuals who follow them on the platform. The current 
plan is to use [Plivo](//www.plivo.com) to manage text messaging, and ~~SendGrid for~~ emails eventually.
    
- [ ] Django User Accounts
- [ ] VueJS - NativeScript
- [ ] Writing Tests!!! (halp!) -Django & VueJS
- [ ] Nginx - anonymize traffic
- [ ] Something other than Google Analytics
- [ ] DB Architecture (PostgreSQL)
- [ ] SEO & Caching 
- [ ] Search: Elastic v Haystack