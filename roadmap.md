# SF Roadmap
ScreamFreely builds provides the educational resources and tools for civic engagement and
tech literacy. In our pursuit of this mission, we must also have tangible goals
and milestones to achieve. This is document will guide and track these aspirations.

## Activist Sites
Presently SF operates Activist sites for 2 states (Minnesota & Illinois), while
owning domain names for 6 additional states. We aspire to populate the remaining 
6 states with event data ASAP. 

### Upcoming Features

- [ ] Org to Follower Communication Paths
- [ ] Org Email & Social Data Scrapes
- [ ] Policy Data for Municipalities
- [ ] Account TimeLine / UX Version 2

### Stage 1
Get each state's Activist site up and running. Providing up-to-date information,
and adding more municipalities daily. Maintian a healthy, respectful, growing and vibrant 
community of users who are willing to convert their opinions into focused data points.

### Stage 2
Federate data across states and begin building data processing pipelines and visualization
mechanisms. Create products and deliverables for individuals and organizations working
in public policy. Nurture an alumni network of former employees working across the sectors.


### Stage 3
Expand internationally