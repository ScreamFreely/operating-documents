[Slack](https://screamfreely.slack.com)
[Twitter](https://twitter.com/screamfreely)
[Instagram](https://instagram.com/screamfreely)
[TeeSpring](https://teespring.com/stores/screamfreely)

# Operating Documents

Contained herein are documents used to guide DreamFreely and DigiSnaxx Labs.
These documents include:

- Code of Conduct
- Communication Protocols
- Call for Support
- Organizational Structure
- Organizational Roadmap


## About ScreamFreely
ScreamFreely is a non-profit created to be an entry point for civic engagement 
and technology. We build and maintain platforms to provide information and
connect communities.

# Projects

## Activist Sites

### CaActivist
[Website](//www.caactivist.org)


### CoActivist
[Website](//www.coactivist.org)


### GaActivist
[Website](//www.gaactivist.org)


### IlActivist
[Website](//www.ilactivist.org)


### MnActivist
[Website](//www.mnactivist.org)
[Twitter](//twitter.com/mnactivist)
[Instagram](//instagram.com/mnactivist)


### NYActivist
[Website](//www.nyactivist.org)


### TXActivist
[Website](//www.txactivist.org)


### VaActivist
[Website](//www.vaactivist.org)


## AdopteesTalk
[Twitter](//twitter.com/adopteestalk)
