# An list of communication preferences
Due to the increased surviellance of all communications, we find it in the 
interests of our endeavors to follow the following to protocols when unable
to communicate in person:

### Video / Audio Conferencing:

- [Jitsi](//meet.jit.si): Jitsi is the preferred platform for video and audio conferencing
- Signal: Signal is satisfactory for brief communiques of meta-info

### Phone & Text Messaging

- Signal: Primary text mechanism
- WhatsApp: Secondary
- Facebook Messenger: Only for scheduling, confirming or joking

### Email

- [Thunderbird]() w/ [Enigmail](https://addons.thunderbird.net/en-US/thunderbird/addon/enigmail/)
- [Claws Mail](https://www.claws-mail.org/)

### Passwords

- [KeePass](https://keepass.info/)
- [KeePassX](https://keepassx.org/)
 
### Server Access

- SSH keys will need to be made by each participant
- Do NOT leave the SSH key password blank
- A VPN key will be made for each participant